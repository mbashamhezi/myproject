<?php include("session1.php");?>
<html>
<body>
<table width="100%" height="100%" align="center" border="0">
	<tr height="10%"><td bgcolor="blue" align="center"><h1>HOSTEL MANAGEMENT SYSTEM</h1></td></tr>
	<tr height="5%" style="box-shadow: 0px 0px 15px cyan;">
		<td bgcolor="silver" valign="top" align="right">
			<table>
				<tr>
					<td><a href="index.php">Home</a></td>
                                        <td><a href="students.php">Manage Students</a></td>
                                        <td><a href="rooms.php">Manage Rooms</a></td>
                                        <td><a href="logout.php">Logout</a></td>
                                </tr>
                        </table>
        <tr>
			<td align="center">
            <a href="available1.php"><button>view Room</button></a><br><br>
            <form action="adding.php" method="post">
          		<h2>Add room</h2>
          			
          				<table>
          					<tr>
                  <td align="left"><label>Room Number:</label></td>
                  <td><input type="number" name="roomno" placeholder="room number"></td>
                </tr>
                   <tr>
                  <td align="left"><label>Room Capacity:</label></td>
                  <td><input type="number" name="capacity" placeholder="room capacity" min="1"></td>
                </tr>
                <tr>
                    <td>For </td>
                    <td><select name="for">
                            <option value="male">Gents</option>
                            <option value="female">Ladies</option>
                        </select></td>
                </tr>
				 <tr>
                  <td align="left"><label>Price Per Bed:</label></td>
                  <td><input type="number" name="price" min="1"></td>
                </tr>
                <tr>
                	<td><input type="submit" name="submit" value="Add"></td>
                </tr>
          				</table>
          			</form>
          		
			</td>
		</tr>
	<tr height="10%">
		<td bgcolor="gray" align="center">Copyright &copy; Hostel MIS <?php echo date("Y");?></td>
	</tr>
</table>	
</body>
</html>

