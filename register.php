<html>
    <body>
        <table width="100%" height="100%" align="center">
            <tr height="10%"><td bgcolor="blue" align="center"><h1>HOSTEL MANAGEMENT SYSTEM</h1></td></tr>
            <tr height="5%" style="box-shadow: 0px 0px 15px cyan;">
                <td bgcolor="silver" valign="top" align="right">
                    <table>
                        <tr>
                            <td><a href="index.php">Home</a></td>
                            <td><a href="about.php">About Us</a></td>
                            <td><a href="contact.php">Contact Us</a></td>
                            <td><a href="food.php">Food</a></td>
                            <td><a href="login.php">Login</a></td>
                        </tr>
                    </table>
                </td>
            <tr>
                <td align="center" valign="top">  
                    <strong><h2>Registration Form</h2></strong>
                    <form action="registering.php" method="post">
                        <table align="center" style="box-shadow: 0px 0px 15px silver;">

                            <tr>

                                <td align="left"><label>Registration No: </label></td>
                                <td><input type="text" name="reg" placeholder="reg number"></td>
                            </tr>
                            <tr>
                                <td align="left"><label>Firstname:</label></td>
                                <td><input type="text" name="fname" placeholder="firstname"></td>
                            </tr>
                            <tr>
                                <td align="left"><label>Middlename:</label></td>
                                <td><input type="text" name="mname" placeholder="middlename"></td>
                            </tr>
                            <tr>
                                <td align="left"><label>Lastname:</label></td>
                                <td><input type="text" name="lname" placeholder="lastname"></td>
                            </tr>
                            </tr>
                            <tr>
                                <td align="left"><label>Gender:</label></td>
                                <td> <select name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select></td>
                            </tr>     

                            <tr>
                                <td align="left"><label>Email:</label></td>
                                <td><input type="email" name="email" placeholder="name@example.com"></td>
                            </tr>

                            <tr>
                                <td align="left"><label>Telephone No:</label></td>
                                <td><input type="text" name="tel" placeholder="07xx-xxx-xxx"></td>
                            </tr>
                            <tr>
                                <td align="left"><label>Password:</label></td>
                                <td><input type="password" name="pass" placeholder="password"></td>
                            </tr>
                            <tr>
                                <td align="left"><label>Retype Password:</label></td>
                                <td><input type="password" name="rpass" placeholder="retype password"></td>
                            </tr>
                            <tr>

                                <td colspan="2"><input type="submit" value="Register"> <a href="stlogin.php"></a></td>
                            </tr>
                        </table>

                    </form></td>
            </tr>
        </tr>
        <tr height="10%">
            <td bgcolor="gray" align="center">Copyright &copy; Hostel MIS <?php echo date("Y"); ?></td>
        </tr>
    </table>	
</body>
</html>
